local composer = require("composer")
local scene    = composer.newScene()
local widget   = require("widget")
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local options = {
    effect = "fromTop",
    time = 400,
    -- params = { }
}

local function handleButtonEvent()
    composer.gotoScene("Screens.GameScreen.GameScreen", options)
end 
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
    local content = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen

    local background = display.newImageRect("assets/General/background.png", 360, 570)
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    content:insert(background)

    local logo = display.newImageRect("assets/Menu/logo.png", 200, 200)
    logo.x = display.contentCenterX
    logo.y = display.contentCenterY - 100
    content:insert(logo)

    local btPlay = widget.newButton({
        width = 160,
        height = 80,
        defaultFile = "assets/Menu/bt_play.png",
        -- overFile = "buttonOver.png",
        onRelease = handleButtonEvent
    })

    btPlay.x = display.contentCenterX
    btPlay.y = display.contentCenterY + 100
    content:insert(btPlay)

    local btRules = display.newImageRect("assets/Menu/bt_rules.png", 160, 80)
    btRules.x = display.contentCenterX
    btRules.y = display.contentCenterY + 190
    content:insert(btRules)
end
 
 
-- show()
function scene:show( event )
    local content = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
    local content = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
    local content = self.view
    -- Code here runs prior to the removal of scene's view
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene