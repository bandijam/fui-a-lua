local composer = require("composer")
local scene    = composer.newScene()
local widget   = require("widget")

local TextStyle = require("General.TextStyle")
local Colors    = require("General.Colors")

local gameplayLoop = require("Screens.GameScreen.gameplayLoop")
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

local gameManager
local ruleText
 
local options = {
    effect = "slideUp",
    time = 400,
    -- params = { }
}

local function handleButtonEvent()
    composer.gotoScene("Screens.HomeScreen.HomeScreen", options)
end 

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
    local content = self.view
    gameManager = gameplayLoop.new()

    local background = display.newImageRect("assets/General/background.png", 360, 570)
    background.x = display.contentCenterX
    background.y = display.contentCenterY
    content:insert(background)

    local holderRule = display.newImageRect("assets/Game/holder_rule.png", 250, 250)
    holderRule.x = display.contentCenterX
    holderRule.y = display.contentCenterY - 20
    content:insert(holderRule)

    ruleText = display.newText(TextStyle.game["rule"])
    ruleText.x = holderRule.x
    ruleText.y = holderRule.y
    ruleText:setFillColor(unpack(Colors["lightBlue"]))
    content:insert(ruleText)

    local holderPlayer = display.newImageRect("assets/Game/holder_player.png", 320, 200)
    holderPlayer.x = display.contentCenterX
    holderPlayer.y = display.contentCenterY - 190
    content:insert(holderPlayer)

    local playerName = display.newText(TextStyle.game["playerName"])
    playerName.text = "Jogador 1"
    playerName.x = holderPlayer.x
    playerName.y = holderPlayer.y
    playerName:setFillColor(unpack(Colors["lightBlue"]))
    content:insert(playerName)

    local btRightAnswer = widget.newButton({
        width = 150,
        height = 150,
        defaultFile = "assets/Game/bt_end_round.png",
        -- overFile = "buttonOver.png",
        onRelease = handleButtonEvent
    })

    btRightAnswer.x = display.contentCenterX
    btRightAnswer.y = display.contentCenterY + 220
    content:insert(btRightAnswer)
end
 
 
-- show()
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        ruleText.text = gameManager.getRule()
 
    elseif ( phase == "did" ) then
 
    end
end
 
 
-- hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene