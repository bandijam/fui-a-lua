local rulesList = {
	[1] = "Apenas coisas começadas pela inicial do seu próprio nome",
	[2] = "Apenas animais do horóscopo chinês",
	[3] = "Qualquer coisa",
	[4] = "Somente objetos que estejam neste ambiente",
	[5] = "Qualquer objeto azul",
	[6] = "Nada",
	[7] = "Apenas coisas começadas pela inicial da pessoa a sua direita",
}



-- ===========
-- = methods =
-- ===========


--@@ timerManagerBuilder()

local function getRule()
	return rulesList[math.random(#rulesList)]
end



-- =========
-- = class =
-- =========

local function new()
	local gameplayLoop = {}

	-- atributes --------------------------



	---------------------------------------

	-- methods ----------------------------

	gameplayLoop.getRule = getRule

	---------------------------------------

	return gameplayLoop
end

local gameplayLoop = {
	new = new
}
-- gameplayLoop.new = new
return gameplayLoop