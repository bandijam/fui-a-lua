local function getColor(r, g, b)
	return { r / 255, g / 255, b / 255 }
end

local colors = {
	["black"]	  = getColor(  0,   0,   0),
	["lightBlue"] = getColor(  0, 128, 255),
	["white"]	  = getColor(255, 255, 255),
}

return colors