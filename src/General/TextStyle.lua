local generalFont = native.systemFont

local textStyles = {
	game = {
		["rule"] = {
			text = "",
	        width = 207,
	        font = native.systemFont,
	        fontSize = 18,
	        align = "center",
	    },
	    ["playerName"] = {
	    	text = "",
	        font = native.systemFont,
	        fontSize = 38,
	        align = "center",
		}
	},
}

return textStyles